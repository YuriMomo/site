+++
title = "This Week In Veloren 6"
description = "Discussions about future issues"

date = 2019-03-11
weight = 0
slug = "devblog-6"
+++

This week has been a slower week on the programming side. More than anything, lots of discussions have occurred to problem solve future issues. Lots of concept art though! This week's blog was written by @AngelOnFira, with an excerpt from @xMAC94x, and an art section curated by @Pfauenauge.

## Programming

The biggest push forward this week were the discussions on character rendering and animation with many entities on screen. Of the talks, some of the big topics include entity caching, and choosing what to render based on occlusion culling.

Also, the netcode is much close to being ready for networked multiplayer. The main task is to get the different modules communicating, and then work on the next steps. There will also be some technical issues to overcome. One is about sending unique information about the player, such as animation progress.

Another heavily discussed topic was world simulation. Since Veloren can be arbitrarily large, it can be difficult to have simulations working at this unknown scale. Here is an excerpt from @xMAC94x,

`
One other question which is still open is how to cover world simulation. Because [Veloren] aims for a really immersive experience, we need to have strong world simulation. That covers everything from wars between cities to vulcanos exploding over groups of birds flying south when it becomes colder. This needs to be calculated on a larger scale. However, this cannot be done in detail everywhere on current hardware. So we need to focus on the area where players are, but not miss out areas with no players. We want a game where a region could change after a week even if a player hasn't visited it. We don't want chunks to be unloaded like in Minecraft and then just not worked on. Because we want the world to constantly be calculated everywhere, we cannot have an infinite world.
`

This current state of the discussion focuses on techniques that could be used to assist with this world simulation. The idea is ambitious, however, an open sourced game has the benefit of trying these new ideas without heavy repercussions.

## Art

<img src="https://images-ext-2.discordapp.net/external/ZC7LqbshfWVpsxGvF4AP59vE_zDIje_VE6SuKn8DWGU/%3Fwidth%3D1201%26height%3D703/https/media.discordapp.net/attachments/481112886308110339/554342570394648593/unknown.png?width=1199&height=702"/>

<img src="https://cdn.discordapp.com/attachments/467073814208053248/554289191102709770/unknown.png"/>

> @Pfauenauge's current concept of the Veloren's UI

<img src="https://cdn.discordapp.com/attachments/449660795857403905/553614126358724619/unknown.png"/>

<img src="https://cdn.discordapp.com/attachments/449660795857403905/553620773034524688/Veloren_icon_quest.png"/>

<img src="https://cdn.discordapp.com/attachments/449660795857403905/553630059609718787/Veloren_icon_quest.png"/>

> @Piedro0's WIP of the quest indicator

<img src="https://cdn.discordapp.com/attachments/449660795857403905/553263588723392533/SushiSchool2.png"/>

> @McKAOS working on some fish concepts

<img src="https://cdn.discordapp.com/attachments/449660795857403905/553217836516900874/unknown.png"/>

> Character window button by @Piedro0 and @Pfauenauge

<img src="https://cdn.discordapp.com/attachments/541307708146581519/554614795488460811/mmap_icons.png"/>

> Toolbar icons by @Demonic, @Piedro0, and @Pfauenauge

<img src="https://media.discordapp.net/attachments/490620627242450955/554300377864994839/unknown.png"/>

> Concept of the spellbook by @Piedro0

## Music

A piece by @Aeronic

### Vast Onslaught
<audio controls>
  <source src="https://cdn.discordapp.com/attachments/449655372618137618/552397271631659032/Vast_Onslaught.ogg" type="audio/ogg">
Your browser does not support the audio element.
</audio>